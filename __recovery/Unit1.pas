unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, unit3,Gauges;

const
  MAX=10;

type
  TForm4 = class(TForm)
    Label1: TLabel;
    btnTest: TButton;
    Edit1: TEdit;
    Timer1: TTimer;
    Shape1: TShape;
    Button1: TButton;
    procedure btnTestClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    private
    list :array[1..MAX] of Integer;   //list

    public
    end;
  TMyObject = class(TObject)            //function
    function test(i:Integer):string;
    end;



var
  Form4: TForm4;

implementation

{$R *.dfm}

//uses Unit3;

function TMyObject.test(i:Integer):string;   //function
begin
  //ShowMessage('12345');
  Result:=i.ToString;    //END的時候才回傳
  //ShowMessage('67890');
end;

 var f:TForm3 =nil;
 var I:integer;
procedure TForm4.btnTestClick(Sender: TObject);
var
  MyObject : TMyObject; //function
  s : string;
  Mylist : integer;     //List
  I:integer;
begin
  MyObject := TMyObject.Create;   //function
  s:=MyObject.test(1);            //function
  //ShowMessage(s);
  self.Hide;
  f := TForm3.Create(nil);
  f.Align:=self.Align;
  f.show


end;

procedure TForm4.Button1Click(Sender: TObject);
begin
  f.Free;
end;


procedure TForm4.FormActivate(Sender: TObject);
begin
  //ShowMessage('Activate');
end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //self.Close;
  //ShowMessage('U1Close');
end;

procedure TForm4.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  //ShowMessage('CloseQuery');
  //f := TForm3.Create(nil);
  //show
end;

procedure TForm4.FormCreate(Sender: TObject);
begin
  //ShowMessage('Create');
end;

procedure TForm4.FormDeactivate(Sender: TObject);
begin
  //ShowMessage('Deactivate');
end;

procedure TForm4.FormDestroy(Sender: TObject);
begin
  //ShowMessage('U1Destroy');
  //form4.Show
  //form4.Free;
end;

procedure TForm4.FormHide(Sender: TObject);
begin
  //ShowMessage('Hide');
end;

procedure TForm4.FormPaint(Sender: TObject);
begin
  //ShowMessage('Paint');
end;

procedure TForm4.FormResize(Sender: TObject);
begin
  //ShowMessage('Resize');
end;

procedure TForm4.FormShow(Sender: TObject);
begin
  //ShowMessage('show');
end;

end.
