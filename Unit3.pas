unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm3 = class(TForm)
    ComboBox1: TComboBox;
    Button1: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;
implementation

{$R *.dfm}

uses Unit1;
procedure TForm3.Button1Click(Sender: TObject);
begin
  //self.Hide;
  label1.Caption:='TEST_A';
end;

procedure TForm3.ComboBox1Change(Sender: TObject);
begin
  ComboBox1.Text:='Test_A';
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //ShowMessage('U3close');
    Form4.Close;
end;

procedure TForm3.FormDestroy(Sender: TObject);
begin
  //ShowMessage('U3Destroy');
  //Form4.Free;
end;

end.
